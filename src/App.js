import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Navigation from "./Components/shared/navigation";
import Home from "./Components/pages/home";
import PatientDetails from "./Components/pages/care-provider/patientDetails";
import PatientSearch from "./Components/pages/care-provider/PatientSearch";
import SearchAdmin from "./Components/pages/super-admin/searchAdmin";
import Admin from "./Components/pages/super-admin/providersAdmin";

function App() {
  return (
    <BrowserRouter>
      <Navigation />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/patient/:id" component={PatientDetails} />
        {/* <Route exact path="/adminDetails" component={AdminDetails} /> */}
        <Route exact path="/patientSearch" component={PatientSearch} />
        <Route exact path="/adminSearch" component={SearchAdmin} />
        <Route exact path="/admin" component={Admin} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
