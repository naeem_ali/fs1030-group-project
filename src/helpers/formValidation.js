const initialValues = {
  email: "",
  password: "",
};

const onSubmit = (values) => {
  console.log("Form Data", values);
};

const validate = (values) => {
  let error = {};

  if (!values.email) {
    error.email = "Required";
  } else if (
    !/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(
      values.email
    )
  ) {
    error.email = "Invalid email format";
  }
  if (!values.password) {
    error.password = "Required";
  }
  return error;
};

export { initialValues, onSubmit, validate };
