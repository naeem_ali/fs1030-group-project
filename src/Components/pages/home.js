import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Col, Row, Button, Form, FormGroup, Label, Input } from "reactstrap";
import { useFormik } from "formik";
import {
  initialValues,
  onSubmit,
  validate,
} from "../../helpers/formValidation";
import "./home.css";

const Home = () => {
  const [auth, setAuth] = useState(true);

  let history = useHistory();

  // login  form validation //

  const formik = useFormik({
    initialValues,
    onSubmit,
    validate,
  });

  const loginSubmit = async (event) => {
    event.preventDefault();
    const email = document.getElementById("usernameEntry").value;
    const password = document.getElementById("passwordEntry").value;

    const response = await fetch("http://localhost:3002/users/auth", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ email, password }),
    });

    const payload = await response.json();
    if (response.status >= 400) {
      setAuth(false);
    } else {
      sessionStorage.setItem("token", payload.token);
      sessionStorage.setItem("permission", payload.permission);
      let permission = sessionStorage.getItem("permission");
      if (permission === "P") {
        history.push("/patientSearch");
      }
      if (permission === "A") {
        history.push("/adminSearch");
      }
    }
  };

 
  return (
    <main className="containerMain">
      {/*homepage background image*/}

      <img src="images/splash.jpg" alt="" />
      <div className="con">
        <div className="containerLogin">
          <h4 className="loginHeader">Login</h4>

          {!auth && (
            <p className="errorMsg">
              <i class="fas fa-exclamation-triangle"></i> Username or password
              is incorrect!
            </p>
          )}

          {/*login form */}
          <Form
            className=" contactForm"

            //  onSubmit={loginSubmit}
          >
            <Row form>
              <Col>
                <FormGroup>
                  <Label for="usernameEntry"></Label>
                  <Input
                    type="text"
                    className="field"
                    name="email"
                    id="usernameEntry"
                    placeholder="Email"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    bsSize="lg"
                  />
                  {formik.touched.email && formik.errors.email ? (
                    <div className="errorMsg-email-l">
                      <span> {formik.errors.email} </span>
                    </div>
                  ) : null}
                </FormGroup>
              </Col>
            </Row>
            <Row>
              <Col>
                <FormGroup>
                  <Label for="passwordEntry"></Label>
                  <Input
                    type="password"
                    className="field"
                    name="password"
                    id="passwordEntry"
                    placeholder="Password"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    bsSize="lg"
                  />
                  {formik.touched.password && formik.errors.password ? (
                    <div className="errorMsg-password">
                      <span> {formik.errors.password}</span>
                    </div>
                  ) : null}
                </FormGroup>
              </Col>
            </Row>
            <Button
              className="loginButton"
              color="warning"
              onClick={loginSubmit}
            >
              <strong>Sign in</strong>
            </Button>
          </Form>
        </div>
      </div>
    </main>
  );
};

export default Home;
