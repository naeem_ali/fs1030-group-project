import React, { useEffect, useState } from 'react'
import { useLocation,useHistory } from "react-router-dom";
import MockData from "../MOCK_DATA.json";
import { Link } from "react-router-dom";
import { Button } from "reactstrap";
import ModalForm from "../../shared/newPatientModal";
import "./style/searchAdmin.css";

const SearchAdmin = () => {
  let history = useHistory();
  const [patients, setPatients] = useState([])
  const [searchTerm, setsearchTerm] = useState("");
  document
  .querySelectorAll("#searchTable button.btn.btn-primary")
  .forEach(function (element) {
    element.addEventListener("click", function (e) {
      let row = this.closest("tr");
      let rowID = row.cells[0].textContent;
      console.log(rowID);
    });
  });
  // const logout = event => {
  //     event.preventDefault()
  //     sessionStorage.removeItem('token')
  //     sessionStorage.removeItem('IsAuth')
  //     history.push("/login")
  // }
  useEffect(() => {
      const getData = async () => {
          const response = await fetch('http://localhost:3002/api/patients', {
              method: 'GET',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              }
          })
          const data = await response.json()
          setPatients(data)
      }
      getData()
  }, [])

  const handleDelete = (event,id) => {
    //event.preventDefault();
    fetch(`http://localhost:3002/api/patient/${id}`, {
      method: "delete",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
    window.location.reload();
    history.push("/adminSearch")
  };

  // const handleView = (event,id) => {
  //   history.push("/patientDetails")
  // };

  const patientRoute = (event, patient) =>{
      // prevent default button behaviour
        event.preventDefault();
        let path = `/patient/${patient.id}`;
        history.push(path);
    };

// const PatientSearch = () => {
//   /*use state search functionality */
//   const [searchTerm, setsearchTerm] = useState("");

//   document
//     .querySelectorAll("#searchTable button.btn.btn-primary")
//     .forEach(function (element) {
//       element.addEventListener("click", function (e) {
//         let row = this.closest("tr");
//         let rowID = row.cells[0].textContent;
//         console.log(rowID);
//       });
//     });

  return (
    <div className="containerMain">
      {/*search container */}
      <div className="searchContainer">
        {/*search input */}
        <div className="searchInput">
          <h1 className="header">Patients</h1>

          <div className="newPatient">
            <ModalForm />
          </div>

          <input
            type="text"
            placeholder="Patient Search"
            className="form-control searchBar"
            onChange={(e) => {
              setsearchTerm(e.target.value);
            }}
          ></input>
        </div>

        {/* patient data table */}
        <table className="tableSearch" id="searchTable">
          <thead className="tableHeadSA">
            <tr>
              <th className="head1">ID</th>
              <th>Full Name</th>
              <th>Email</th>
              <th>Address</th>
              <th>City</th>
              <th>Province</th>
              <th>Postal Code</th>
              <th>Gender</th>
              <th>Blood Type</th>
              <th>Select</th>
              <th className="head2">Delete</th>
            </tr>
          </thead>
          <tbody>
            {/* load table data */}
            {patients.filter((value) => {
              if (searchTerm === "") {
                return value;
              } else if (
                value.name
                  .toLowerCase()
                  .includes(searchTerm.toLowerCase()) ||
                value.email.toLowerCase().includes(searchTerm.toLowerCase())
              ) {
                return value;
              }
            }).map((patient) => (
              <tr key={patient.id}>
                <td>{patient.id}</td>
                <td>{patient.name}</td>
                <td>{patient.email}</td>
                <td>{patient.address}</td>
                <td>{patient.city}</td>
                <td>{patient.province}</td>
                <td>{patient.postal_code}</td>
                <td>{patient.Gender}</td>
                <td>{patient.blood_type}</td>
                <td>
                  <button onClick={(event) => patientRoute(event, patient)} className="btn btn-primary">View</button>
                </td>
                <td>
                  <button onClick={(event) => {handleDelete(event,patient.id);}} className="btn btn-danger">Delete</button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default SearchAdmin;