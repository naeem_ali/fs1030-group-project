import React from "react";
import MockData from "../MOCK_DATA.json";
import { useState, useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import { Button, Table } from "reactstrap";
import ProvModalForm from "../../shared/super-admin/newProviderModal";
import ProvUpdateModalForm from "../../shared/super-admin/updateProviderModal";
import "./style/providersAdmin.css";
import moment from "moment";

const Admin = () => {
  let history = useHistory();
  const [searchTerm, setsearchTerm] = useState("");
  /*  const token = sessionStorage.getItem('token')
   const us er = parseJwt(token).username*/
  const [providers, setProviders] = useState([])
  /*   const logout = event => {
      event.preventDefault()
      sessionStorage.removeItem('token')
      history.push("/login")
    } */
  useEffect(() => {
    const getProviders = async () => {
      const response = await fetch('http://localhost:3002/api/providers', {
        method: 'GET',
        /*         headers: {
                  'Authorization': `Bearer ${token}`
                } */
      })
      const data = await response.json()
      //console.log(data);
      setProviders(data)
      //console.log(listing)
    }
    getProviders()
  })
  //[token])

  const handleDelete = (event,id) => {
    event.preventDefault();
    fetch(`http://localhost:3002/api/provider/${id}`, {
      method: "delete",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
    window.location.reload();
  };

  return (
    <div className="containerMain">
      {/*search container */}
      <div className="searchContainer">
        {/*search input */}
        <div className="searchInput">
          <h1 className="header">Care Providers</h1>

          <div className="newPatient">
            <ProvModalForm />
          </div>

          <input
            type="text"
            placeholder="Provider Search"
            className="form-control searchBar"
            onChange={(e) => {
              setsearchTerm(e.target.value);
            }}
          ></input>
          <h3> </h3>
          <div className="searchInputProv">
            <table className="tableSearch">
              <thead className="tableHeadSA">
                <tr>
                  <th>Employee ID</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Designation</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
            {/* load table data */}
            {providers.filter((value) => {
              if (searchTerm === "") {
                return value;
              } else if (
                value.name
                  .toLowerCase()
                  .includes(searchTerm.toLowerCase()) ||
                value.email.toLowerCase().includes(searchTerm.toLowerCase())
              ) {
                return value;
              }
            }).map((m) => (
              <tr key={m.id}>
                <td>{m.id}</td>
                <td>{m.name}</td>
                <td>{m.email}</td>
                <td>{m.phone}</td>
                <td>{m.designation}</td>
                <td>
                  <button onClick={(e) => {handleDelete(e,m.id);}} className="btn btn-danger">Delete</button>
                </td>
              </tr>
            ))}
          </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Admin;
