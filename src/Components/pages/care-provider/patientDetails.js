import React, { useState, useEffect } from "react";
import {
  TabContent,
  Table,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
} from "reactstrap";
import classnames from "classnames";
import UpdateModalForm from "../../shared/updatePatientModal";
import "./Style/patientDetails.css";

const PatientDetails = (props) => {

let id = props.match.params.id;

const [patients, setPatients] = useState([]);

  const [activeTab, setActiveTab] = useState("1");

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  useEffect(()=> {
      async function fetchData() {
          const res = await fetch(`http://localhost:3002/api/patientdetails/${id}`);
          res
          .json()
          .then((res) => setPatients(res))
          .catch((err) => console.log(err))
      }
      fetchData();
  }, [id])  

  return (
    <div className="containerMain">
      {/*details container */}

      <div className="detailDisplay">

        {/*details patient card display */}

        <h1 className="detailsHeadTitle">Patient Details</h1>
        
        <div className="detailsHeader">

          {patients.map((patient) => (
                    <div key={patient.id}>
             
          <p>Patient ID : {patient.patient_id}</p>    
           <p>Patient Name : {patient.patientname}</p>  
            <p>Patient DOB : {patient.DOB}</p>  
             <p>Patient Gender : {patient.Gender}</p>  
              <p>Patient Blood Type : {patient.blood_type}</p>  
               <p>Patient Health Card Number : {patient.health_card_number}</p>  
                                  

      </div>
      
           ))}

           
      </div>


        <div className="updateButton">
             <UpdateModalForm />
        </div>


        {/*tabs container */}
       
       <Nav tabs>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '1' })}
            onClick={() => { toggle('1'); }}
          >
            <strong>Contact Details</strong>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '2' })}
            onClick={() => { toggle('2'); }}
          >
           <strong>Visits</strong>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '3' })}
            onClick={() => { toggle('3'); }}
          >
           <strong>Lab Tests</strong>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '4' })}
            onClick={() => { toggle('4'); }}
          >
           <strong>Medical History</strong>
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '5' })}
            onClick={() => { toggle('5'); }}
          >
           <strong>Billing and Payments</strong>
          </NavLink>
        </NavItem>
        <NavItem>
        <NavItem>
          <NavLink
            className={classnames({ active: activeTab === '6' })}
            onClick={() => { toggle('6'); }}
          >
           <strong>Notes</strong>
          </NavLink>
        </NavItem>     

              
          
        </NavItem>

      </Nav>      
      
      <TabContent activeTab={activeTab}>
        <TabPane tabId="1"> 
          <Row>
                         
              <h4 className="detailsTitle">Contact Details</h4>
              
              {patients.map((patient) => (
          
              <div key={patient.id} className="detailsBox">
                <p>Email: {patient.email} </p>
                <p>Address: {patient.address} </p>
                <p>Postal Code: {patient.postal_code} </p>
                <p>Province: {patient.province} </p>
              </div>       ))}      
            
          </Row>
        </TabPane>
        <TabPane tabId="2">
          <Row>
            <h4 className="detailsTitle">Visits</h4>

            {patients.map((patient) => (

            <div key={patient.id} className="detailsBox">

            <Table>
                <thead>
                  <tr>
                    
                    <th>Date and Time</th>
                    <th>Reason</th>
                    <th>Diagnosis</th>
                    <th>Bill No.</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                   
                    <td>{patient.date_time}</td>
                    <td>{patient.visitreason}</td>
                    <td>{patient.diagnosis}</td>
                    <td>{patient.bill_number}</td>
                  </tr>
                  
                 
                  
                </tbody>
            </Table>
             
            </div>
            ))}
            
          </Row>
        </TabPane>
        <TabPane tabId="3">
          <Row>
            
            <h4 className="detailsTitle">Tests</h4>

            {patients.map((patient) => (

            <div key={patient.id} className="detailsBox">
              
              <Table>
                <thead>
                  <tr>
                    
                    <th>Date and Time</th>
                    <th>Test Name</th>
                    <th>Results</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    
                    <td>{patient.date_time}</td>
                    <td>{patient.testname}</td>
                    <td>{patient.results}</td>
                  </tr>
                </tbody>
            </Table>


            </div>
            ))}
          </Row>
        </TabPane>
        <TabPane tabId="4">
          <Row>
          
            <h4 className="detailsTitle">History</h4>

            {patients.map((patient) => (

            <div key={patient.id} className="detailsBox">
              
              <Table>
                <thead>
                  <tr>
                   
                    <th>Physical Exam</th>
                    <th>Medication</th>
                    <th>Immunization Status</th>
                    <th>Surgery</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                   
                    <td>{patient.physical_exams}</td>
                    <td>{patient.medicines_taken}</td>
                    <td>{patient.immunization_status}</td>
                    <td>{patient.surgeries}</td>
                  </tr>
                </tbody>
            </Table>


            </div>
            ))}
          </Row>
        </TabPane>
        <TabPane tabId="5">
          <Row>
          <h4 className="detailsTitle">Bills</h4>

           {patients.map((patient) => (

           <div key={patient.id} className="detailsBox">
              
            <Table>
                <thead>
                  <tr>
                    <th>Bill No.</th>
                    <th>Status</th>
                    <th>Bill Details</th>
                    <th>Amount</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    
                    <td>{patient.bill_number}</td>
                    <td>{patient.status}</td>
                    <td>{patient.bill_details}</td>
                    <td>{patient.billamount}</td>
                  </tr>
                </tbody>
            </Table>

            </div>
            ))}
          </Row>
          <Row>
          <h4 className="detailsTitle">Payments</h4>

           {patients.map((patient) => (

           <div key={patient.id} className="detailsBox">
              
            <Table>
                <thead>
                  <tr>
                    <th>Reciept No.</th>
                    <th>Status</th>
                    <th>Amount Paid</th>
                    
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>{patient.bill_number}</td>
                    <td>{patient.status}</td>
                    <td>{patient.billamount}</td>
                  </tr>
                </tbody>
            </Table>
              
            </div>

            ))}
          </Row>
        </TabPane>
        <TabPane tabId="6">
          <Row>
          <h4 className="detailsTitle">Notes</h4>

           {patients.map((patient) => (

           <div key={patient.id} className="detailsBox">
              <p>
                {patient.notes}
              </p>
              
            </div>

           ))}
          </Row>
        </TabPane>
      </TabContent>    




      </div>
    </div>
    
  );
};

export default PatientDetails;
