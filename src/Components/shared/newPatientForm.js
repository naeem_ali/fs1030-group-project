import React, { useState } from 'react';
// import { Button,  Form, FormGroup, Label, Input, Col, Input, Label, Button, Container, CardBody, Card, CardText, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { useHistory } from "react-router-dom";
import { Form, FormGroup, DropdownMenu, Col, Input, Label, Button, Container, CardBody, Card, CardText } from 'reactstrap'

const NewPatientForm = () => {
    const [name, setName] = useState("")
    const [email, setEmail] = useState("")
    const [address, setAddress] = useState("")
    const [city, setCity] = useState("")
    const [province, setProvince] = useState("")
    const [postal_code, setPostcode] = useState("")
    const [health_card_number, setHealthcard] = useState("")
    const [blood_type, setBloodType] = useState("")
    const [DOB, setDOB] = useState("")
    const [Gender, setGender] = useState("")
    const [success, setSuccess] = useState(false)
    const [failure, setFailure] = useState(false)
    let history = useHistory();
    const formSubmit = async event => {
        //event.preventDefault()
        const response = await fetch('http://localhost:3002/api/patient', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({name, email, address, city, province, postal_code, health_card_number, blood_type, DOB, Gender })
        })
        if (response.status >= 400) {
            setFailure(true)
        } else {
            setSuccess(true)
        }
    }
    
    return(
        
    <Container>
            {success && 
                <Card className="text-white bg-primary my-1 py-1 text-center">
                    <CardBody>
                        <CardText className="text-white m-0"><div className="alert">Patient Added Successfully</div></CardText>
                    </CardBody>
                </Card>
            }
            {failure &&
                <Card className="text-white bg-danger my-1 py-1 text-center">
                    <CardBody>
                        <CardText className="text-white m-0"><div className="alert">Oops! something went wrong, try again after sometime</div></CardText>
                    </CardBody>
                </Card>
            }
        <Form onSubmit={formSubmit}>
                <FormGroup>
                    <Label for="patientName">Full Name</Label>
                    <Input 
                        type="text"
                        name="patientName" 
                        id="patientName" 
                        placeholder="Full name"
                        required value={name}
                        onChange={e => setName(e.target.value)}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="exampleEmail">Email</Label>
                    <Input 
                        type="email" 
                        name="email"
                        id="exampleEmail"
                        placeholder="Primary email to contact"
                        required value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="patientAdd">Address</Label>
                    <Input 
                        type="text" 
                        name="patientAdd"
                        id="patientAdd"
                        placeholder="Address"
                        required value={address}
                        onChange={e => setAddress(e.target.value)}
                     />
                </FormGroup>
                <FormGroup>
                    <Label for="patientCity">City</Label>
                    <Input 
                        type="text"
                        name="patientCity"
                        id="patientCity"
                        placeholder="City"
                        required value={city}
                        onChange={e => setCity(e.target.value)}
                      />
                </FormGroup>
                <FormGroup>
                    <Label for="patientProv">Province</Label>
                    <Input 
                        type="text" 
                        name="patientProv" 
                        id="patientProv" 
                        placeholder="Province"
                        required value={province}
                        onChange={e => setProvince(e.target.value)}
                    />
                </FormGroup>
                <FormGroup>
                    <Label for="patientPC">Postal Code</Label>
                    <Input 
                        type="text" 
                        name="patientPC"
                        id="patientPC"
                        placeholder="A1A 1A1"
                        required value={postal_code}
                        onChange={e => setPostcode(e.target.value)}
                     />
                </FormGroup>
                <FormGroup>
                    <Label for="patientHC">Health Card</Label>
                    <Input 
                        type="text" 
                        name="patientHC" 
                        id="patientHC"
                        placeholder="xxxx-xxx-xxx-AA"
                        required value={health_card_number}
                        onChange={e => setHealthcard(e.target.value)}
                     />
                </FormGroup>
                <FormGroup>
                    <Label for="patientBlood">Blood Type</Label>
                    <Input 
                        type="text" 
                        name="patientBlood" 
                        id="patientBlood"
                        placeholder="eg: O +ive"
                        required value={blood_type}
                        onChange={e => setBloodType(e.target.value)}
                     />
                </FormGroup>
                <FormGroup>
                    <Label for="patientDOB">Date of Birth</Label>
                    <Input 
                        type="date"
                        name="patientDOB"
                        id="patientDOB" 
                        placeholder="1901-01-01"
                        required value={DOB}
                        onChange={e => setDOB(e.target.value)}
                      />
                </FormGroup>
                <FormGroup>
                    <Label for="patientGender">Gender</Label>
                    <Input 
                        type="text" 
                        name="patientGender" 
                        id="patientGender"
                        placeholder="Gender"
                        required value={Gender}
                        onChange={e => setGender(e.target.value)}
                     />
                </FormGroup>
                <h3> </h3>
                <FormGroup check row>
                    <Col sm={{ size: 10, offset: 2 }}>
                    <div className="button"><Button disabled={success || failure} color="success">Submit</Button></div>
                    </Col>
                </FormGroup>
            </Form>
        </Container>    

  )

}

export default NewPatientForm

            