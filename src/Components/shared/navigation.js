import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Container,
  Button,
} from "reactstrap";
import { useLocation, useHistory } from "react-router-dom";
import { NavLink as RouteLink } from "react-router-dom";
import "./nav.css";

const Navigation = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const location = useLocation();
  const history = useHistory();

  const logout = () => {
    history.push("/");
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("permission");
  };

  const permission = sessionStorage.getItem("permission");

  const isAdmin = location.pathname === "/adminSearch" || permission === "A";
  const isProvider =
    location.pathname === "/patientSearch" || permission === "P";

  return (
    <Navbar className="nav-con" dark color="dark" expand="md" fixed="top">
      <Container>
        <NavbarBrand href="">
          <span className="logo">&#9877;&#65039;</span> EMR System
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            {/* <NavItem>
              <NavLink tag={RouteLink} to="/">
                Home
              </NavLink>
            </NavItem> */}
            {isProvider && (
              <>
                <NavItem>
                  <NavLink tag={RouteLink} to="/patientSearch">
                    Search Patients
                  </NavLink>
                </NavItem>
                {/* <NavItem>
                  <NavLink tag={RouteLink} to="/patientDetails">
                    Patient Details
                  </NavLink>
                </NavItem> */}
                <NavItem className="logout-btn">
                  <Button className="logout" color="danger" onClick={logout}>
                    Logout
                  </Button>
                </NavItem>
              </>
            )}
            {!isAdmin && (
              <>
                <NavItem>
                  <NavLink tag={RouteLink} to="/adminSearch">
                    *SA* Search Patients
                  </NavLink>
                </NavItem>

                <NavItem>
                  <NavLink tag={RouteLink} to="/admin">
                    *SA* Providers
                  </NavLink>
                </NavItem>
                <NavItem className="logout-btn">
                  <Button className="logout" color="danger" onClick={logout}>
                    Logout
                  </Button>
                </NavItem>
              </>
            )}
          </Nav>
        </Collapse>
      </Container>
    </Navbar>
  );
};

export default Navigation;
