import React, { useState } from 'react';
import { Button,  Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import NewPatientForm from './newPatientForm';

const ModalForm = () => {
 
    

  const [modal, setModal] = useState(false);

//   
  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color="danger" onClick={toggle}>New Patient</Button>
      <Modal isOpen={modal} toggle={toggle} >
        <ModalHeader toggle={toggle}>Create New Patient</ModalHeader>
        
        <ModalBody> 
          
          <NewPatientForm />
        
        </ModalBody>

       
      </Modal>
    </div>
  );
}

export default ModalForm;