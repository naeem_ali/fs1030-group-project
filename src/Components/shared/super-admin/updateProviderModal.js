import React, { useState } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import UpdateProviderForm from './updateProviderForm';

const UpdateModalForm = (props) => {

  const {
    buttonLabel,
    className
  } = props;

  const [modal, setModal] = useState(false);

  //   
  const toggle = () => setModal(!modal);

  return (
    <div className="updateModal">
      <Button color="primary" onClick={toggle}>Edit</Button>
      <Modal isOpen={modal} toggle={toggle} contentClassName="custom-modal-style" className={className}>
        <ModalHeader toggle={toggle}>Update ProviderDetails</ModalHeader>

        <ModalBody>

          <UpdateProviderForm curprovider={props.provider} />

        </ModalBody>


      </Modal>
    </div>
  );
}

export default UpdateModalForm;