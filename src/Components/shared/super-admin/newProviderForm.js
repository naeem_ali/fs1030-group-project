import React, { useState } from 'react';
import { Button, Form, Col, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { useHistory } from "react-router-dom";
import moment from 'moment';

const NewProviderForm = () => {

    const [newProvider, setNewProvider] = useState({
        name: "",
        email: "",
        phone: 0,
        start_date: moment().format("YYYY-MM-DD"),
        end_date: moment().format("YYYY-MM-DD"),
        status: "A",
        designation: "",
        permission: "P",
        password: "",
    });


    const history = useHistory();
    const [success, setSuccess] = useState(false);
    const [failure, setFailure] = useState(false);

    const handleChange = (event) => {
        setNewProvider((prevState) => ({
            ...prevState,
            [event.target.name]: event.target.value,
        }));
    };

    const handleClose = () => {
        //event.target.setState({ modal: false });
        history.push("/admin")
    }

    const handleSubmit = (event) => {
        console.log("log data in submit")
        //event.preventDefault();
        console.log("test" + event.submitter);
        //console.log({ ...newProvider });
        fetch("http://localhost:3002/users/register", {
            method: "post",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            //make sure to serialize your JSON body
            body: JSON.stringify(newProvider),
        }).then(response => response.json());
        history.push("/admin");
    };

    return (


        <Form onSubmit={handleSubmit}>
            <FormGroup>
                <Label for="name">Full Name</Label>
                <Input
                    type="text"
                    name="name"
                    id="name"
                    value={newProvider.name}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="email">Email</Label>
                <Input
                    type="email"
                    name="email"
                    id="email"
                    value={newProvider.email}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="password">Password</Label>
                <Input
                    type="password"
                    name="password"
                    id="password"
                    value={newProvider.password}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="phone">Phone</Label>
                <Input
                    type="text"
                    name="phone"
                    id="phone"
                    value={newProvider.phone}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="start_date">Start Date</Label>
                <Input
                    type="date"
                    name="start_date"
                    id="start_date"
                    value={newProvider.start_date}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="end_date">End Date</Label>
                <Input
                    type="date"
                    name="end_date"
                    id="end_date"
                    value={newProvider.end_date}
                    onChange={handleChange}
                />
            </FormGroup>
            {/*             <FormGroup>
                <Label for="status">Status</Label>
                <Input
                    type="text"
                    name="status"
                    id="status"
                    value={newProvider.status}
                    onChange={handleChange}
                />
            </FormGroup> */}
            <FormGroup>
                <Label for="designation">Designation</Label>
                <Input
                    type="text"
                    name="designation"
                    id="designation"
                    value={newProvider.designation}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="permission">Role</Label>
                {/* <Input
                    type="text"
                    name="permission"
                    id="permission"
                    value={newProvider.permission}
                    onChange={handleChange}
                /> */}
                <Input type="select" name="permission" id="permission"
                    value={newProvider.permission}
                    onChange={handleChange}>
                    <option value="P">Provider</option>
                    <option value="A">Admin</option>
                </Input>

                {/*                 <Form.Select name="permission" id="permission" value={newProvider.permission} onChange={handleChange}>
                    <option value="P">Provider</option>
                    <option value="A">Admin</option>
                </Form.Select> */}
            </FormGroup>
            {/* <Button type="submit" value="Submit" />
            <Button type="button" value="Cancel" onClick={handleClose} />*/}
            {/* <input type="submit" value="Submit" /> */}
            {/* <input type="button" value="Cancel" onClick={handleClose} /> */}
            <h3> </h3>
            <FormGroup check row>
                <Col sm={{ size: 10, offset: 2 }}>
                <div className="button"><Button disabled={success || failure} color="success">Submit</Button></div>
                </Col>
            </FormGroup>
        </Form>


    )

}

export default NewProviderForm

