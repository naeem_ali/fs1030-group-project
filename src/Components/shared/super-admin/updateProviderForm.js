import React, { useState } from 'react';
import { Button, Col, Row, Form, FormGroup, Label, Input, FormText, Modal, ModalHeader, ModalBody, ModalFooter, Collapse, CardBody, Card } from 'reactstrap';
import { useHistory, useParams } from "react-router-dom";
import moment from "moment";

const UpdateProviderForm = (props) => {

    console.log(props.curprovider);

    const id = props.curprovider.id;

    const [updateProvider, setProvider] = useState({
        name: props.curprovider.name,
        email: props.curprovider.email,
        phone: props.curprovider.phone,
        start_date: moment(props.curprovider.start_date).format("YYYY-MM-DD"),
        end_date: moment(props.curprovider.end_date).format("YYYY-MM-DD"),
        status: props.curprovider.status,
        designation: props.curprovider.designation,
        permission: props.curprovider.permission,
        password: props.curprovider.password,
    });


    const history = useHistory();
    const params = useParams();

    const handleChange = (event) => {
        setProvider((prevState) => ({
            ...prevState,
            [event.target.name]: event.target.value,
        }));
    };

    const handleSubmit = (event) => {
        //console.log("log data in submit")
        //event.preventDefault();
        console.log({ ...updateProvider });
        fetch(`http://localhost:3002/users/user/${id}`, {
            method: "post",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            //make sure to serialize your JSON body
            body: JSON.stringify(updateProvider),
        }).then(response => response.json());
        history.push("/admin");
    };

    return (


        <Form onSubmit={handleSubmit}>
            <FormGroup>
                <Label for="name">Full Name</Label>
                <Input
                    type="text"
                    name="name"
                    id="name"
                    value={updateProvider.name}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="email">Email</Label>
                <Input
                    type="email"
                    name="email"
                    id="email"
                    value={updateProvider.email}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="password">Password</Label>
                <Input
                    type="password"
                    name="password"
                    id="password"
                    value={updateProvider.password}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="phone">Phone</Label>
                <Input
                    type="text"
                    name="phone"
                    id="phone"
                    value={updateProvider.phone}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="start_date">Start Date</Label>
                <Input
                    type="date"
                    name="start_date"
                    id="start_date"
                    value={moment(updateProvider.start_date).format("YYYY-MM-DD")}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="end_date">End Date</Label>
                <Input
                    type="date"
                    name="end_date"
                    id="end_date"
                    value={moment(updateProvider.end_date).format("YYYY-MM-DD")}
                    onChange={handleChange}
                />
            </FormGroup>
            {/*             <FormGroup>
                <Label for="status">Status</Label>
                <Input
                    type="text"
                    name="status"
                    id="status"
                    value={updateProvider.status}
                    onChange={handleChange}
                />
            </FormGroup> */}
            <FormGroup>
                <Label for="designation">Designation</Label>
                <Input
                    type="text"
                    name="designation"
                    id="designation"
                    value={updateProvider.designation}
                    onChange={handleChange}
                />
            </FormGroup>
            <FormGroup>
                <Label for="permission">Role</Label>
                <Input type="select" name="permission" id="permission"
                    value={updateProvider.permission}
                    onChange={handleChange}>
                    <option value="P">Provider</option>
                    <option value="A">Admin</option>
                </Input>

                {/*                 <Input
                    type="text"
                    name="permission"
                    id="permission"
                    value={updateProvider.permission}
                    onChange={handleChange}
                /> */}
            </FormGroup>
            <input type="submit" value="Submit" />
            {/* <input type="submit" value="Cancel" /> */}
        </Form>

    )
}

export default UpdateProviderForm

